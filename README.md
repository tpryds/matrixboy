# MatrixBoy

Simple handheld game device powered by an Arduino Nano or compatible, featuring a 8x16 pixel 1-bit LED Matrix display, to be mounted in a NES controller casing.

