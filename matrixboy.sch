<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Thomas">
<packages>
<package name="LED_8X8MATRIX_DRIVERCARD">
<pad name="VCC@1" x="-5.08" y="-15.24" drill="0.9"/>
<pad name="GND@1" x="-2.54" y="-15.24" drill="0.9"/>
<pad name="DIN" x="0" y="-15.24" drill="0.9"/>
<pad name="CS@1" x="2.54" y="-15.24" drill="0.9"/>
<pad name="CLK@1" x="5.08" y="-15.24" drill="0.9"/>
<text x="-15.24" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.08" y="-13.97" size="0.8128" layer="21" rot="R90" align="center-left">VCC</text>
<text x="-2.54" y="-13.97" size="0.8128" layer="21" rot="R90" align="center-left">GND</text>
<text x="0" y="-13.97" size="0.8128" layer="21" rot="R90" align="center-left">DIN</text>
<text x="2.54" y="-13.97" size="0.8128" layer="21" rot="R90" align="center-left">CS</text>
<text x="5.08" y="-13.97" size="0.8128" layer="21" rot="R90" align="center-left">CLK</text>
<pad name="CLK@2" x="5.08" y="15.24" drill="0.9"/>
<pad name="CS@2" x="2.54" y="15.24" drill="0.9"/>
<pad name="DOUT" x="0" y="15.24" drill="0.9"/>
<pad name="GND@2" x="-2.54" y="15.24" drill="0.9"/>
<pad name="VCC@2" x="-5.08" y="15.24" drill="0.9"/>
<text x="5.08" y="13.97" size="0.8128" layer="21" rot="R90" align="center-right">CLK</text>
<text x="2.54" y="13.97" size="0.8128" layer="21" rot="R90" align="center-right">CS</text>
<text x="0" y="13.97" size="0.8128" layer="21" rot="R90" align="center-right">DOUT</text>
<text x="-2.54" y="13.97" size="0.8128" layer="21" rot="R90" align="center-right">GND</text>
<text x="-5.08" y="13.97" size="0.8128" layer="21" rot="R90" align="center-right">VCC</text>
<wire x1="16.51" y1="16.51" x2="16.51" y2="-16.51" width="0.127" layer="21"/>
<wire x1="-16.51" y1="16.51" x2="16.51" y2="16.51" width="0.127" layer="21"/>
<wire x1="-16.51" y1="16.51" x2="-16.51" y2="-16.51" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-16.51" x2="16.51" y2="-16.51" width="0.127" layer="21"/>
<rectangle x1="-16.51" y1="-16.51" x2="16.51" y2="16.51" layer="39"/>
</package>
<package name="ARDUINO_NANO_KINA">
<pad name="D13" x="1.27" y="-1.27" drill="0.9"/>
<pad name="3V3" x="1.27" y="-3.81" drill="0.9"/>
<pad name="REF" x="1.27" y="-6.35" drill="0.9"/>
<pad name="A0" x="1.27" y="-8.89" drill="0.9"/>
<pad name="A1" x="1.27" y="-11.43" drill="0.9"/>
<pad name="A2" x="1.27" y="-13.97" drill="0.9"/>
<pad name="A3" x="1.27" y="-16.51" drill="0.9"/>
<pad name="A4" x="1.27" y="-19.05" drill="0.9"/>
<pad name="A5" x="1.27" y="-21.59" drill="0.9"/>
<pad name="A6" x="1.27" y="-24.13" drill="0.9"/>
<pad name="A7" x="1.27" y="-26.67" drill="0.9"/>
<pad name="5V" x="1.27" y="-29.21" drill="0.9"/>
<pad name="RST1" x="1.27" y="-31.75" drill="0.9"/>
<pad name="GND1" x="1.27" y="-34.29" drill="0.9"/>
<pad name="VIN" x="1.27" y="-36.83" drill="0.9"/>
<pad name="TX1" x="16.51" y="-36.83" drill="0.9" shape="square"/>
<pad name="RX0" x="16.51" y="-34.29" drill="0.9"/>
<pad name="RST2" x="16.51" y="-31.75" drill="0.9"/>
<pad name="GND2" x="16.51" y="-29.21" drill="0.9"/>
<pad name="D2" x="16.51" y="-26.67" drill="0.9"/>
<pad name="D3" x="16.51" y="-24.13" drill="0.9"/>
<pad name="D4" x="16.51" y="-21.59" drill="0.9"/>
<pad name="D5" x="16.51" y="-19.05" drill="0.9"/>
<pad name="D6" x="16.51" y="-16.51" drill="0.9"/>
<pad name="D7" x="16.51" y="-13.97" drill="0.9"/>
<pad name="D8" x="16.51" y="-11.43" drill="0.9"/>
<pad name="D9" x="16.51" y="-8.89" drill="0.9"/>
<pad name="D10" x="16.51" y="-6.35" drill="0.9"/>
<pad name="D11" x="16.51" y="-3.81" drill="0.9"/>
<pad name="D12" x="16.51" y="-1.27" drill="0.9"/>
<pad name="ICSP_5V" x="6.35" y="-36.83" drill="0.9"/>
<pad name="ICSP_MOSI" x="8.89" y="-36.83" drill="0.9"/>
<pad name="ICSP_GND" x="11.43" y="-36.83" drill="0.9"/>
<pad name="ICSP_MISO" x="6.35" y="-39.37" drill="0.9" shape="square"/>
<pad name="ICSP_SCK" x="8.89" y="-39.37" drill="0.9"/>
<pad name="ICSP_RESET" x="11.43" y="-39.37" drill="0.9"/>
<wire x1="0" y1="-40.64" x2="0" y2="3.81" width="0.127" layer="21"/>
<wire x1="17.78" y1="3.81" x2="17.78" y2="-40.64" width="0.127" layer="21"/>
<wire x1="17.78" y1="-40.64" x2="12.7" y2="-40.64" width="0.127" layer="21"/>
<rectangle x1="0" y1="-40.64" x2="17.78" y2="5.08" layer="39"/>
<wire x1="12.7" y1="-40.64" x2="5.08" y2="-40.64" width="0.127" layer="21"/>
<wire x1="5.08" y1="-40.64" x2="0" y2="-40.64" width="0.127" layer="21"/>
<wire x1="5.08" y1="-40.64" x2="5.08" y2="-35.56" width="0.127" layer="21"/>
<wire x1="5.08" y1="-35.56" x2="12.7" y2="-35.56" width="0.127" layer="21"/>
<wire x1="12.7" y1="-35.56" x2="12.7" y2="-40.64" width="0.127" layer="21"/>
<wire x1="5.08" y1="-3.81" x2="12.7" y2="-3.81" width="0.127" layer="21"/>
<wire x1="12.7" y1="-3.81" x2="12.7" y2="3.81" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.81" x2="12.7" y2="5.08" width="0.127" layer="21"/>
<wire x1="12.7" y1="5.08" x2="5.08" y2="5.08" width="0.127" layer="21"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="-3.81" width="0.127" layer="21"/>
<wire x1="0" y1="3.81" x2="5.08" y2="3.81" width="0.127" layer="21"/>
<wire x1="12.7" y1="3.81" x2="17.78" y2="3.81" width="0.127" layer="21"/>
<wire x1="8.89" y1="-7.62" x2="3.81" y2="-12.7" width="0.127" layer="21"/>
<wire x1="3.81" y1="-12.7" x2="8.89" y2="-17.78" width="0.127" layer="21"/>
<wire x1="8.89" y1="-17.78" x2="13.97" y2="-12.7" width="0.127" layer="21"/>
<wire x1="13.97" y1="-12.7" x2="8.89" y2="-7.62" width="0.127" layer="21"/>
<text x="8.89" y="-20.32" size="1.27" layer="21" align="bottom-center">NANO</text>
<text x="15.24" y="-36.83" size="1.016" layer="21" align="center-right">TX1</text>
<text x="15.24" y="-34.29" size="1.016" layer="21" align="center-right">RX0</text>
<text x="15.24" y="-31.75" size="1.016" layer="21" align="center-right">RST</text>
<text x="15.24" y="-29.21" size="1.016" layer="21" align="center-right">GND</text>
<text x="15.24" y="-26.67" size="1.016" layer="21" align="center-right">D2</text>
<text x="15.24" y="-24.13" size="1.016" layer="21" align="center-right">D3</text>
<text x="15.24" y="-21.59" size="1.016" layer="21" align="center-right">D4</text>
<text x="15.24" y="-19.05" size="1.016" layer="21" align="center-right">D5</text>
<text x="15.24" y="-16.51" size="1.016" layer="21" align="center-right">D6</text>
<text x="15.24" y="-13.97" size="1.016" layer="21" align="center-right">D7</text>
<text x="15.24" y="-11.43" size="1.016" layer="21" align="center-right">D8</text>
<text x="15.24" y="-8.89" size="1.016" layer="21" align="center-right">D9</text>
<text x="15.24" y="-6.35" size="1.016" layer="21" align="center-right">D10</text>
<text x="15.24" y="-3.81" size="1.016" layer="21" align="center-right">D11</text>
<text x="15.24" y="-1.27" size="1.016" layer="21" align="center-right">D12</text>
<text x="2.54" y="-1.27" size="1.016" layer="21" align="center-left">D13</text>
<text x="2.54" y="-3.81" size="1.016" layer="21" align="center-left">3V3</text>
<text x="2.54" y="-6.35" size="1.016" layer="21" align="center-left">REF</text>
<text x="2.54" y="-8.89" size="1.016" layer="21" align="center-left">A0</text>
<text x="2.54" y="-11.43" size="1.016" layer="21" align="center-left">A1</text>
<text x="2.54" y="-13.97" size="1.016" layer="21" align="center-left">A2</text>
<text x="2.54" y="-16.51" size="1.016" layer="21" align="center-left">A3</text>
<text x="2.54" y="-19.05" size="1.016" layer="21" align="center-left">A4</text>
<text x="2.54" y="-21.59" size="1.016" layer="21" align="center-left">A5</text>
<text x="2.54" y="-24.13" size="1.016" layer="21" align="center-left">A6</text>
<text x="2.54" y="-26.67" size="1.016" layer="21" align="center-left">A7</text>
<text x="2.54" y="-29.21" size="1.016" layer="21" align="center-left">5V</text>
<text x="2.54" y="-31.75" size="1.016" layer="21" align="center-left">RST</text>
<text x="2.54" y="-34.29" size="1.016" layer="21" align="center-left">GND</text>
<text x="2.54" y="-36.83" size="1.016" layer="21" align="center-left">VIN</text>
<text x="1.27" y="-43.18" size="1.27" layer="21">&gt;NAME</text>
</package>
<package name="USB_MINI-B_5PF_90DEG_THRUHOLE">
<description>Mini USB Type B 5PF
Female Connector Receptacle SMT/DIP 90°Angled/Tail Board PCB Socket
Ebay/China</description>
<pad name="3" x="0" y="0" drill="0.7" diameter="1.016"/>
<pad name="4" x="0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="5" x="1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="2" x="-0.8" y="-1.2" drill="0.7" diameter="1.016"/>
<pad name="1" x="-1.6" y="0" drill="0.7" diameter="1.016"/>
<pad name="SHIELD@1" x="-3.65" y="-5.6" drill="1.8"/>
<pad name="SHIELD@2" x="3.65" y="-5.6" drill="1.8"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-9.525" x2="3.81" y2="-9.525" width="0.127" layer="21"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.127" layer="21"/>
<rectangle x1="-3.81" y1="-9.525" x2="3.81" y2="0" layer="39"/>
<text x="5.08" y="-8.89" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="RUBBERBUTTON_CONTACTAREA_9MM">
<circle x="0" y="0" radius="4.699" width="0.127" layer="21"/>
<text x="3.81" y="-3.81" size="1.27" layer="25" align="top-left">&gt;NAME</text>
<smd name="A" x="-5.08" y="0" dx="1.27" dy="1.27" layer="1" roundness="100"/>
<smd name="B" x="5.08" y="0" dx="1.27" dy="1.27" layer="1" roundness="100"/>
<circle x="0" y="0" radius="4.445" width="0" layer="29"/>
<circle x="0" y="0" radius="4.572" width="0" layer="39"/>
<polygon width="0.127" layer="1">
<vertex x="-3.175" y="0"/>
<vertex x="2.54" y="0"/>
<vertex x="2.54" y="0.635"/>
<vertex x="-3.175" y="0.635"/>
<vertex x="-2.54" y="2.54"/>
<vertex x="3.175" y="2.54"/>
<vertex x="2.54" y="3.175"/>
<vertex x="-2.54" y="3.175"/>
<vertex x="-3.175" y="2.54"/>
<vertex x="-3.556" y="2.032"/>
<vertex x="-3.81" y="1.27"/>
<vertex x="-3.81" y="0.508"/>
<vertex x="-5.08" y="0.508"/>
<vertex x="-5.08" y="-0.635"/>
<vertex x="-3.81" y="-0.635"/>
<vertex x="-3.81" y="-1.651"/>
<vertex x="-3.175" y="-2.54"/>
<vertex x="1.524" y="-2.54"/>
<vertex x="1.905" y="-1.905"/>
<vertex x="-2.54" y="-1.905"/>
<vertex x="-3.175" y="-1.27"/>
</polygon>
<polygon width="0.127" layer="1">
<vertex x="-2.54" y="-1.27"/>
<vertex x="3.175" y="-1.27"/>
<vertex x="1.905" y="-3.175"/>
<vertex x="-2.54" y="-3.175"/>
<vertex x="-1.524" y="-3.81"/>
<vertex x="1.778" y="-3.81"/>
<vertex x="2.921" y="-3.048"/>
<vertex x="3.556" y="-2.159"/>
<vertex x="3.81" y="-1.27"/>
<vertex x="3.81" y="-0.635"/>
<vertex x="5.08" y="-0.635"/>
<vertex x="5.08" y="0.635"/>
<vertex x="3.81" y="0.635"/>
<vertex x="3.81" y="1.27"/>
<vertex x="3.683" y="1.905"/>
<vertex x="-1.905" y="1.905"/>
<vertex x="-2.159" y="1.27"/>
<vertex x="3.175" y="1.27"/>
<vertex x="3.175" y="-0.635"/>
<vertex x="-2.54" y="-0.635"/>
</polygon>
</package>
<package name="RESISTOR_THRUHOLE">
<pad name="1" x="-3.81" y="0" drill="0.7"/>
<pad name="2" x="3.81" y="0" drill="0.7"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<text x="0" y="2.54" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="27" align="top-center">&gt;VALUE</text>
<rectangle x1="-2.54" y1="-1.27" x2="2.54" y2="1.27" layer="39"/>
<rectangle x1="-3.81" y1="-0.635" x2="-2.54" y2="0.635" layer="39"/>
<rectangle x1="2.54" y1="-0.635" x2="3.81" y2="0.635" layer="39"/>
</package>
<package name="TOGGLESWITCH_SPDT_90DEG_2MMPITCH">
<pad name="A" x="-2" y="0" drill="0.9" shape="offset" rot="R270"/>
<pad name="N" x="0" y="0" drill="0.9" shape="offset" rot="R270"/>
<pad name="B" x="2" y="0" drill="0.9" shape="offset" rot="R270"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<rectangle x1="-5.08" y1="-2.54" x2="5.08" y2="2.54" layer="39"/>
<text x="-5.08" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="5.08" width="0.127" layer="25"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.127" layer="25"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.127" layer="25"/>
<pad name="GND@1" x="-4" y="0" drill="1.3" shape="square"/>
<pad name="GND@2" x="4" y="0" drill="1.3" shape="square"/>
<rectangle x1="-2.54" y1="2.54" x2="2.54" y2="6.35" layer="39"/>
</package>
<package name="TOGGLESWITCH_SPDT_90DEG_3MMPITCH">
<pad name="A" x="-3" y="0" drill="1" shape="offset" rot="R270"/>
<pad name="N" x="0" y="0" drill="1" shape="offset" rot="R270"/>
<pad name="B" x="3" y="0" drill="1" shape="offset" rot="R270"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.127" layer="21"/>
<wire x1="6.35" y1="6.35" x2="6.35" y2="0" width="0.127" layer="21"/>
<wire x1="6.35" y1="0" x2="-6.35" y2="0" width="0.127" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="6.35" width="0.127" layer="21"/>
<text x="-5.08" y="-5.08" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="10.16" width="0.127" layer="25"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.127" layer="25"/>
<wire x1="-1.27" y1="10.16" x2="-1.27" y2="6.35" width="0.127" layer="25"/>
<pad name="GND@1" x="-6" y="0" drill="1.3" shape="square"/>
<pad name="GND@2" x="6" y="0" drill="1.3" shape="square"/>
<rectangle x1="-3.175" y1="6.35" x2="3.175" y2="10.16" layer="39"/>
<rectangle x1="-6.35" y1="0" x2="6.35" y2="6.35" layer="39"/>
</package>
<package name="BUZZER_ROUND_6.6MMPITCH">
<pad name="PLUS" x="3.3" y="0" drill="0.9" shape="square"/>
<pad name="MINUS" x="-3.3" y="0" drill="0.9"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<text x="3.175" y="-1.905" size="1.778" layer="21" align="center">+</text>
<circle x="0" y="0" radius="6.35" width="0" layer="39"/>
<text x="0" y="1.27" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<circle x="0" y="0" radius="0.635" width="0.127" layer="21"/>
</package>
<package name="BUZZER_ROUND_STDPITCH">
<pad name="PLUS" x="3.81" y="0" drill="0.9" shape="square"/>
<pad name="MINUS" x="-3.81" y="0" drill="0.9"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<text x="3.175" y="-1.905" size="1.778" layer="21" align="center">+</text>
<circle x="0" y="0" radius="6.35" width="0" layer="39"/>
<text x="0" y="1.27" size="1.27" layer="25" align="bottom-center">&gt;NAME</text>
<circle x="0" y="0" radius="0.635" width="0.127" layer="21"/>
</package>
<package name="ARDUINO_NANO_KINA_UNDERSIDE_MOUNT">
<pad name="D13" x="1.27" y="-1.27" drill="0.9"/>
<pad name="3V3" x="1.27" y="-3.81" drill="0.9"/>
<pad name="REF" x="1.27" y="-6.35" drill="0.9"/>
<pad name="A0" x="1.27" y="-8.89" drill="0.9"/>
<pad name="A1" x="1.27" y="-11.43" drill="0.9"/>
<pad name="A2" x="1.27" y="-13.97" drill="0.9"/>
<pad name="A3" x="1.27" y="-16.51" drill="0.9"/>
<pad name="A4" x="1.27" y="-19.05" drill="0.9"/>
<pad name="A5" x="1.27" y="-21.59" drill="0.9"/>
<pad name="A6" x="1.27" y="-24.13" drill="0.9"/>
<pad name="A7" x="1.27" y="-26.67" drill="0.9"/>
<pad name="5V" x="1.27" y="-29.21" drill="0.9"/>
<pad name="RST1" x="1.27" y="-31.75" drill="0.9"/>
<pad name="GND1" x="1.27" y="-34.29" drill="0.9"/>
<pad name="VIN" x="1.27" y="-36.83" drill="0.9"/>
<pad name="TX1" x="16.51" y="-36.83" drill="0.9" shape="square"/>
<pad name="RX0" x="16.51" y="-34.29" drill="0.9"/>
<pad name="RST2" x="16.51" y="-31.75" drill="0.9"/>
<pad name="GND2" x="16.51" y="-29.21" drill="0.9"/>
<pad name="D2" x="16.51" y="-26.67" drill="0.9"/>
<pad name="D3" x="16.51" y="-24.13" drill="0.9"/>
<pad name="D4" x="16.51" y="-21.59" drill="0.9"/>
<pad name="D5" x="16.51" y="-19.05" drill="0.9"/>
<pad name="D6" x="16.51" y="-16.51" drill="0.9"/>
<pad name="D7" x="16.51" y="-13.97" drill="0.9"/>
<pad name="D8" x="16.51" y="-11.43" drill="0.9"/>
<pad name="D9" x="16.51" y="-8.89" drill="0.9"/>
<pad name="D10" x="16.51" y="-6.35" drill="0.9"/>
<pad name="D11" x="16.51" y="-3.81" drill="0.9"/>
<pad name="D12" x="16.51" y="-1.27" drill="0.9"/>
<pad name="ICSP_5V" x="6.35" y="-36.83" drill="0.9"/>
<pad name="ICSP_MOSI" x="8.89" y="-36.83" drill="0.9"/>
<pad name="ICSP_GND" x="11.43" y="-36.83" drill="0.9"/>
<pad name="ICSP_MISO" x="6.35" y="-39.37" drill="0.9" shape="square"/>
<pad name="ICSP_SCK" x="8.89" y="-39.37" drill="0.9"/>
<pad name="ICSP_RESET" x="11.43" y="-39.37" drill="0.9"/>
<wire x1="0" y1="-40.64" x2="0" y2="3.81" width="0.127" layer="22"/>
<wire x1="17.78" y1="3.81" x2="17.78" y2="-40.64" width="0.127" layer="22"/>
<wire x1="17.78" y1="-40.64" x2="12.7" y2="-40.64" width="0.127" layer="22"/>
<wire x1="12.7" y1="-40.64" x2="5.08" y2="-40.64" width="0.127" layer="22"/>
<wire x1="5.08" y1="-40.64" x2="0" y2="-40.64" width="0.127" layer="22"/>
<wire x1="5.08" y1="-40.64" x2="5.08" y2="-35.56" width="0.127" layer="22"/>
<wire x1="5.08" y1="-35.56" x2="12.7" y2="-35.56" width="0.127" layer="22"/>
<wire x1="12.7" y1="-35.56" x2="12.7" y2="-40.64" width="0.127" layer="22"/>
<wire x1="0" y1="3.81" x2="5.08" y2="3.81" width="0.127" layer="22"/>
<wire x1="12.7" y1="3.81" x2="17.78" y2="3.81" width="0.127" layer="22"/>
<wire x1="8.89" y1="-7.62" x2="3.81" y2="-12.7" width="0.127" layer="22"/>
<wire x1="3.81" y1="-12.7" x2="8.89" y2="-17.78" width="0.127" layer="22"/>
<wire x1="8.89" y1="-17.78" x2="13.97" y2="-12.7" width="0.127" layer="22"/>
<wire x1="13.97" y1="-12.7" x2="8.89" y2="-7.62" width="0.127" layer="22"/>
<text x="8.89" y="-19.05" size="1.27" layer="22" rot="MR0" align="bottom-center">NANO</text>
<text x="15.24" y="-36.83" size="1.016" layer="22" rot="MR0" align="center-left">TX1</text>
<text x="15.24" y="-34.29" size="1.016" layer="22" rot="MR0" align="center-left">RX0</text>
<text x="15.24" y="-31.75" size="1.016" layer="22" rot="MR0" align="center-left">RST</text>
<text x="15.24" y="-29.21" size="1.016" layer="22" rot="MR0" align="center-left">GND</text>
<text x="15.24" y="-26.67" size="1.016" layer="22" rot="MR0" align="center-left">D2</text>
<text x="15.24" y="-24.13" size="1.016" layer="22" rot="MR0" align="center-left">D3</text>
<text x="15.24" y="-21.59" size="1.016" layer="22" rot="MR0" align="center-left">D4</text>
<text x="15.24" y="-19.05" size="1.016" layer="22" rot="MR0" align="center-left">D5</text>
<text x="15.24" y="-16.51" size="1.016" layer="22" rot="MR0" align="center-left">D6</text>
<text x="15.24" y="-13.97" size="1.016" layer="22" rot="MR0" align="center-left">D7</text>
<text x="15.24" y="-11.43" size="1.016" layer="22" rot="MR0" align="center-left">D8</text>
<text x="15.24" y="-8.89" size="1.016" layer="22" rot="MR0" align="center-left">D9</text>
<text x="15.24" y="-6.35" size="1.016" layer="22" rot="MR0" align="center-left">D10</text>
<text x="16.002" y="-2.667" size="1.016" layer="22" rot="MR0" align="center-left">D11</text>
<text x="16.002" y="-0.127" size="1.016" layer="22" rot="MR0" align="center-left">D12</text>
<text x="1.778" y="-0.127" size="1.016" layer="22" rot="MR0" align="center-right">D13</text>
<text x="1.778" y="-2.667" size="1.016" layer="22" rot="MR0" align="center-right">3V3</text>
<text x="2.54" y="-6.35" size="1.016" layer="22" rot="MR0" align="center-right">REF</text>
<text x="2.54" y="-8.89" size="1.016" layer="22" rot="MR0" align="center-right">A0</text>
<text x="2.54" y="-11.43" size="1.016" layer="22" rot="MR0" align="center-right">A1</text>
<text x="2.54" y="-13.97" size="1.016" layer="22" rot="MR0" align="center-right">A2</text>
<text x="2.54" y="-16.51" size="1.016" layer="22" rot="MR0" align="center-right">A3</text>
<text x="2.54" y="-19.05" size="1.016" layer="22" rot="MR0" align="center-right">A4</text>
<text x="2.54" y="-21.59" size="1.016" layer="22" rot="MR0" align="center-right">A5</text>
<text x="2.54" y="-24.13" size="1.016" layer="22" rot="MR0" align="center-right">A6</text>
<text x="2.54" y="-26.67" size="1.016" layer="22" rot="MR0" align="center-right">A7</text>
<text x="2.54" y="-29.21" size="1.016" layer="22" rot="MR0" align="center-right">5V</text>
<text x="2.54" y="-31.75" size="1.016" layer="22" rot="MR0" align="center-right">RST</text>
<text x="2.54" y="-34.29" size="1.016" layer="22" rot="MR0" align="center-right">GND</text>
<text x="2.54" y="-36.83" size="1.016" layer="22" rot="MR0" align="center-right">VIN</text>
<text x="16.51" y="-43.18" size="1.27" layer="22" rot="MR0">&gt;NAME</text>
<wire x1="4.445" y1="5.08" x2="4.445" y2="-5.08" width="0.127" layer="20"/>
<wire x1="4.445" y1="-5.08" x2="13.335" y2="-5.08" width="0.127" layer="20"/>
<wire x1="13.335" y1="-5.08" x2="13.335" y2="5.08" width="0.127" layer="20"/>
<wire x1="13.335" y1="5.08" x2="4.445" y2="5.08" width="0.127" layer="51"/>
<wire x1="4.445" y1="-20.32" x2="13.335" y2="-20.32" width="0.127" layer="20"/>
<wire x1="13.335" y1="-20.32" x2="13.335" y2="-25.4" width="0.127" layer="20"/>
<wire x1="13.335" y1="-25.4" x2="4.445" y2="-25.4" width="0.127" layer="20"/>
<wire x1="4.445" y1="-25.4" x2="4.445" y2="-20.32" width="0.127" layer="20"/>
<text x="8.89" y="-22.86" size="0.8128" layer="51" align="center">Reset btn
cutout</text>
<text x="8.89" y="0" size="0.8128" layer="51" align="center">USB
cutout</text>
<text x="8.89" y="-29.21" size="0.8128" layer="21" align="center">Mount Nano
to other side
without flipping it</text>
</package>
<package name="BUZZER_ROUND_6.6MMPITCH_VERTICALMOUNT">
<text x="1.905" y="-0.635" size="1.778" layer="21" align="center">+</text>
<text x="0" y="2.921" size="1.27" layer="25" rot="R90" align="center-right">&gt;NAME</text>
<smd name="MINUS" x="-3.3" y="0" dx="1.27" dy="5.08" layer="1"/>
<smd name="PLUS" x="3.3" y="0" dx="1.27" dy="5.08" layer="1"/>
<wire x1="-6.35" y1="3.81" x2="6.35" y2="3.81" width="0" layer="20"/>
<wire x1="-6.35" y1="3.81" x2="-6.35" y2="12.7" width="0" layer="20"/>
<wire x1="6.35" y1="3.81" x2="6.35" y2="12.7" width="0" layer="20"/>
<text x="0" y="8.89" size="0.8128" layer="51" align="center">Buzzer
cutout</text>
<wire x1="-2.54" y1="12.7" x2="-6.35" y2="12.7" width="0" layer="20"/>
<wire x1="2.54" y1="12.7" x2="6.35" y2="12.7" width="0" layer="20"/>
<wire x1="-5.715" y1="3.81" x2="-5.715" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="5.715" y2="3.175" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.175" x2="5.715" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="12.7" x2="2.54" y2="12.7" width="0" layer="20" curve="-90"/>
</package>
</packages>
<symbols>
<symbol name="LED_8X8MATRIX_W/DRIVERCARD_INOUT">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="4.064" width="0.254" layer="94"/>
<wire x1="2.54" y1="4.064" x2="1.524" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.524" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="4.064" x2="-1.016" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.064" width="0.254" layer="94"/>
<wire x1="-1.016" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<pin name="VCC" x="-5.08" y="-15.24" visible="pin" length="middle" rot="R90"/>
<pin name="GND" x="0" y="-15.24" visible="pin" length="middle" rot="R90"/>
<pin name="DIN" x="5.08" y="-15.24" visible="pin" length="middle" rot="R90"/>
<pin name="CS" x="-5.08" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="CLK" x="5.08" y="15.24" visible="pin" length="middle" rot="R270"/>
<text x="12.7" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<pin name="DOUT" x="0" y="15.24" visible="pin" length="middle" rot="R270"/>
</symbol>
<symbol name="ARDUINO_NANO">
<pin name="D13" x="-5.08" y="-2.54" visible="pin" length="middle"/>
<pin name="3V3" x="-5.08" y="-5.08" visible="pin" length="middle"/>
<pin name="AREF" x="-5.08" y="-7.62" visible="pin" length="middle"/>
<pin name="A0" x="-5.08" y="-27.94" visible="pin" length="middle"/>
<pin name="A1" x="-5.08" y="-25.4" visible="pin" length="middle"/>
<pin name="A2" x="-5.08" y="-22.86" visible="pin" length="middle"/>
<pin name="A3" x="-5.08" y="-20.32" visible="pin" length="middle"/>
<pin name="A4" x="-5.08" y="-17.78" visible="pin" length="middle"/>
<pin name="A5" x="-5.08" y="-15.24" visible="pin" length="middle"/>
<pin name="A6" x="-5.08" y="-12.7" visible="pin" length="middle"/>
<pin name="A7" x="-5.08" y="-10.16" visible="pin" length="middle"/>
<pin name="+5V" x="-5.08" y="-30.48" visible="pin" length="middle"/>
<pin name="RESET1" x="-5.08" y="-33.02" visible="pin" length="middle"/>
<pin name="GND1" x="-5.08" y="-35.56" visible="pin" length="middle"/>
<pin name="VIN" x="-5.08" y="-38.1" visible="pin" length="middle"/>
<pin name="D1/TX" x="27.94" y="-38.1" visible="pin" length="middle" rot="R180"/>
<pin name="D0/RX" x="27.94" y="-35.56" visible="pin" length="middle" rot="R180"/>
<pin name="RESET2" x="27.94" y="-33.02" visible="pin" length="middle" rot="R180"/>
<pin name="GND2" x="27.94" y="-30.48" visible="pin" length="middle" rot="R180"/>
<pin name="D2" x="27.94" y="-27.94" visible="pin" length="middle" rot="R180"/>
<pin name="D3" x="27.94" y="-25.4" visible="pin" length="middle" rot="R180"/>
<pin name="D4" x="27.94" y="-22.86" visible="pin" length="middle" rot="R180"/>
<pin name="D5" x="27.94" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="D6" x="27.94" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="D7" x="27.94" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="D8" x="27.94" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="D9" x="27.94" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="D10" x="27.94" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="D11" x="27.94" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="D12" x="27.94" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="ICSP_MISO" x="5.08" y="-58.42" visible="pin" length="middle" rot="R90"/>
<pin name="ICSP_SCK" x="7.62" y="-58.42" visible="pin" length="middle" rot="R90"/>
<pin name="ICSP_RESET" x="10.16" y="-58.42" visible="pin" length="middle" rot="R90"/>
<pin name="ICSP_GND" x="12.7" y="-58.42" visible="pin" length="middle" rot="R90"/>
<pin name="ICSP_MOSI" x="15.24" y="-58.42" visible="pin" length="middle" rot="R90"/>
<pin name="ICSP_5V" x="17.78" y="-58.42" visible="pin" length="middle" rot="R90"/>
<wire x1="0" y1="0" x2="22.86" y2="0" width="0.254" layer="94"/>
<wire x1="22.86" y1="0" x2="22.86" y2="-53.34" width="0.254" layer="94"/>
<wire x1="22.86" y1="-53.34" x2="0" y2="-53.34" width="0.254" layer="94"/>
<wire x1="0" y1="-53.34" x2="0" y2="0" width="0.254" layer="94"/>
<text x="12.7" y="-25.4" size="1.778" layer="95" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="USB_CONN">
<pin name="1_VCC" x="-7.62" y="5.08" visible="pin"/>
<pin name="2_D-" x="-7.62" y="2.54" visible="pin"/>
<pin name="3_D+" x="-7.62" y="0" visible="pin"/>
<pin name="4_ID" x="-7.62" y="-2.54" visible="pin"/>
<pin name="5_GND" x="-7.62" y="-5.08" visible="pin"/>
<wire x1="-2.54" y1="6.35" x2="1.27" y2="7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.254" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="10.16" x2="3.81" y2="-10.16" width="0.254" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="3.81" y2="10.16" width="0.254" layer="94"/>
<pin name="SHIELD" x="-2.54" y="-10.16" visible="pin" length="short"/>
<text x="-5.08" y="10.668" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="PUSHBUTTON_NORMALLY_OPEN">
<circle x="2.54" y="0" radius="0.762" width="0.254" layer="94"/>
<pin name="A" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="B" x="7.62" y="0" visible="off" length="middle" rot="R180"/>
<circle x="-2.54" y="0" radius="0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="6.35" width="0.254" layer="94"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<pin name="1" x="-7.62" y="0" visible="off" length="short"/>
<pin name="2" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="2.54" size="1.778" layer="95" align="bottom-center">&gt;NAME</text>
<text x="0" y="-2.54" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="GND">
<pin name="GND" x="0" y="0" visible="off" length="short" rot="R270"/>
<wire x1="-1.27" y1="-3.81" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-0.254" y1="-5.08" x2="0.254" y2="-5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="TOGGLESWITCH_SPDT">
<circle x="2.54" y="5.08" radius="0.762" width="0.254" layer="94"/>
<pin name="N" x="-7.62" y="0" visible="off" length="middle"/>
<pin name="A" x="7.62" y="5.08" visible="off" length="middle" rot="R180"/>
<circle x="-2.54" y="0" radius="0.762" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="3.556" y2="4.572" width="0.254" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.762" width="0.254" layer="94"/>
<pin name="B" x="7.62" y="-5.08" visible="off" length="middle" rot="R180"/>
<text x="-2.54" y="2.54" size="1.778" layer="95" align="bottom-right">&gt;NAME</text>
</symbol>
<symbol name="BUZZER_2PINS">
<pin name="PLUS" x="5.08" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="MINUS" x="5.08" y="-2.54" visible="pad" length="middle" rot="R180"/>
<wire x1="-7.62" y1="7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94" curve="-180"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_8X8MATRIX_DRIVERCARD" prefix="LED">
<gates>
<gate name="G$1" symbol="LED_8X8MATRIX_W/DRIVERCARD_INOUT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED_8X8MATRIX_DRIVERCARD">
<connects>
<connect gate="G$1" pin="CLK" pad="CLK@1 CLK@2"/>
<connect gate="G$1" pin="CS" pad="CS@1 CS@2"/>
<connect gate="G$1" pin="DIN" pad="DIN"/>
<connect gate="G$1" pin="DOUT" pad="DOUT"/>
<connect gate="G$1" pin="GND" pad="GND@1 GND@2"/>
<connect gate="G$1" pin="VCC" pad="VCC@1 VCC@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ARDUINO_NANO_KINA" prefix="IC">
<gates>
<gate name="G$1" symbol="ARDUINO_NANO" x="0" y="40.64"/>
</gates>
<devices>
<device name="" package="ARDUINO_NANO_KINA">
<connects>
<connect gate="G$1" pin="+5V" pad="5V"/>
<connect gate="G$1" pin="3V3" pad="3V3"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="AREF" pad="REF"/>
<connect gate="G$1" pin="D0/RX" pad="RX0"/>
<connect gate="G$1" pin="D1/TX" pad="TX1"/>
<connect gate="G$1" pin="D10" pad="D10"/>
<connect gate="G$1" pin="D11" pad="D11"/>
<connect gate="G$1" pin="D12" pad="D12"/>
<connect gate="G$1" pin="D13" pad="D13"/>
<connect gate="G$1" pin="D2" pad="D2"/>
<connect gate="G$1" pin="D3" pad="D3"/>
<connect gate="G$1" pin="D4" pad="D4"/>
<connect gate="G$1" pin="D5" pad="D5"/>
<connect gate="G$1" pin="D6" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="D8" pad="D8"/>
<connect gate="G$1" pin="D9" pad="D9"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="ICSP_5V" pad="ICSP_5V"/>
<connect gate="G$1" pin="ICSP_GND" pad="ICSP_GND"/>
<connect gate="G$1" pin="ICSP_MISO" pad="ICSP_MISO"/>
<connect gate="G$1" pin="ICSP_MOSI" pad="ICSP_MOSI"/>
<connect gate="G$1" pin="ICSP_RESET" pad="ICSP_RESET"/>
<connect gate="G$1" pin="ICSP_SCK" pad="ICSP_SCK"/>
<connect gate="G$1" pin="RESET1" pad="RST1"/>
<connect gate="G$1" pin="RESET2" pad="RST2"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="UNDERSIDE_MOUNT" package="ARDUINO_NANO_KINA_UNDERSIDE_MOUNT">
<connects>
<connect gate="G$1" pin="+5V" pad="5V"/>
<connect gate="G$1" pin="3V3" pad="3V3"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="AREF" pad="REF"/>
<connect gate="G$1" pin="D0/RX" pad="RX0"/>
<connect gate="G$1" pin="D1/TX" pad="TX1"/>
<connect gate="G$1" pin="D10" pad="D10"/>
<connect gate="G$1" pin="D11" pad="D11"/>
<connect gate="G$1" pin="D12" pad="D12"/>
<connect gate="G$1" pin="D13" pad="D13"/>
<connect gate="G$1" pin="D2" pad="D2"/>
<connect gate="G$1" pin="D3" pad="D3"/>
<connect gate="G$1" pin="D4" pad="D4"/>
<connect gate="G$1" pin="D5" pad="D5"/>
<connect gate="G$1" pin="D6" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="D8" pad="D8"/>
<connect gate="G$1" pin="D9" pad="D9"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="ICSP_5V" pad="ICSP_5V"/>
<connect gate="G$1" pin="ICSP_GND" pad="ICSP_GND"/>
<connect gate="G$1" pin="ICSP_MISO" pad="ICSP_MISO"/>
<connect gate="G$1" pin="ICSP_MOSI" pad="ICSP_MOSI"/>
<connect gate="G$1" pin="ICSP_RESET" pad="ICSP_RESET"/>
<connect gate="G$1" pin="ICSP_SCK" pad="ICSP_SCK"/>
<connect gate="G$1" pin="RESET1" pad="RST1"/>
<connect gate="G$1" pin="RESET2" pad="RST2"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB" prefix="CN">
<gates>
<gate name="G$1" symbol="USB_CONN" x="2.54" y="0"/>
</gates>
<devices>
<device name="MINI-B_5PF_90DEG_THRUHOLE" package="USB_MINI-B_5PF_90DEG_THRUHOLE">
<connects>
<connect gate="G$1" pin="1_VCC" pad="1"/>
<connect gate="G$1" pin="2_D-" pad="2"/>
<connect gate="G$1" pin="3_D+" pad="3"/>
<connect gate="G$1" pin="4_ID" pad="4"/>
<connect gate="G$1" pin="5_GND" pad="5"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD@1 SHIELD@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RUBBERBUTTON_CONTACTAREA_9MM" prefix="S">
<gates>
<gate name="G$1" symbol="PUSHBUTTON_NORMALLY_OPEN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RUBBERBUTTON_CONTACTAREA_9MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR_THRUHOLE" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="THRUHOLE" package="RESISTOR_THRUHOLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" prefix="S">
<gates>
<gate name="G$2" symbol="GND" x="0" y="17.78"/>
<gate name="G$1" symbol="TOGGLESWITCH_SPDT" x="0" y="5.08"/>
</gates>
<devices>
<device name="" package="TOGGLESWITCH_SPDT_90DEG_2MMPITCH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$2" pin="GND" pad="GND@1 GND@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="TOGGLESWITCH_SPDT_90DEG_3MMPITCH">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="N" pad="N"/>
<connect gate="G$2" pin="GND" pad="GND@1 GND@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUZZER_2PINS" prefix="LS">
<gates>
<gate name="G$1" symbol="BUZZER_2PINS" x="2.54" y="0"/>
</gates>
<devices>
<device name="-6.6MM_PITCH" package="BUZZER_ROUND_6.6MMPITCH">
<connects>
<connect gate="G$1" pin="MINUS" pad="MINUS"/>
<connect gate="G$1" pin="PLUS" pad="PLUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-STD_PITCH" package="BUZZER_ROUND_STDPITCH">
<connects>
<connect gate="G$1" pin="MINUS" pad="MINUS"/>
<connect gate="G$1" pin="PLUS" pad="PLUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6.6MM_PITCH_VERTICAL_MOUNT" package="BUZZER_ROUND_6.6MMPITCH_VERTICALMOUNT">
<connects>
<connect gate="G$1" pin="MINUS" pad="MINUS"/>
<connect gate="G$1" pin="PLUS" pad="PLUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0.381" drill="0.508">
<clearance class="0" value="0.3048"/>
</class>
</classes>
<parts>
<part name="LED_TOP" library="Thomas" deviceset="LED_8X8MATRIX_DRIVERCARD" device=""/>
<part name="LED_BOTTOM" library="Thomas" deviceset="LED_8X8MATRIX_DRIVERCARD" device=""/>
<part name="USB" library="Thomas" deviceset="USB" device="MINI-B_5PF_90DEG_THRUHOLE"/>
<part name="RIGHT" library="Thomas" deviceset="RUBBERBUTTON_CONTACTAREA_9MM" device=""/>
<part name="LEFT" library="Thomas" deviceset="RUBBERBUTTON_CONTACTAREA_9MM" device=""/>
<part name="DOWN" library="Thomas" deviceset="RUBBERBUTTON_CONTACTAREA_9MM" device=""/>
<part name="UP" library="Thomas" deviceset="RUBBERBUTTON_CONTACTAREA_9MM" device=""/>
<part name="R1" library="Thomas" deviceset="RESISTOR_THRUHOLE" device="THRUHOLE" value="10K"/>
<part name="R2" library="Thomas" deviceset="RESISTOR_THRUHOLE" device="THRUHOLE" value="10K"/>
<part name="R3" library="Thomas" deviceset="RESISTOR_THRUHOLE" device="THRUHOLE" value="10K"/>
<part name="R4" library="Thomas" deviceset="RESISTOR_THRUHOLE" device="THRUHOLE" value="10K"/>
<part name="AUDIO_SW" library="Thomas" deviceset="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" device=""/>
<part name="NANO" library="Thomas" deviceset="ARDUINO_NANO_KINA" device="UNDERSIDE_MOUNT"/>
<part name="PWR_SW" library="Thomas" deviceset="TOGGLESWITCH_SPDT_90DEG_2MMPITCH" device=""/>
<part name="BUZZ" library="Thomas" deviceset="BUZZER_2PINS" device="-6.6MM_PITCH_VERTICAL_MOUNT"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="LED_TOP" gate="G$1" x="60.96" y="25.4" smashed="yes">
<attribute name="NAME" x="73.66" y="33.02" size="1.27" layer="95"/>
</instance>
<instance part="LED_BOTTOM" gate="G$1" x="60.96" y="73.66" smashed="yes">
<attribute name="NAME" x="73.66" y="81.28" size="1.27" layer="95"/>
</instance>
<instance part="USB" gate="G$1" x="-129.54" y="43.18" smashed="yes">
<attribute name="NAME" x="-134.62" y="53.848" size="1.778" layer="95"/>
</instance>
<instance part="RIGHT" gate="G$1" x="20.32" y="86.36" smashed="yes">
<attribute name="NAME" x="22.86" y="91.44" size="1.778" layer="95"/>
</instance>
<instance part="LEFT" gate="G$1" x="20.32" y="76.2" smashed="yes">
<attribute name="NAME" x="22.86" y="81.28" size="1.778" layer="95"/>
</instance>
<instance part="DOWN" gate="G$1" x="20.32" y="66.04" smashed="yes">
<attribute name="NAME" x="22.86" y="71.12" size="1.778" layer="95"/>
</instance>
<instance part="UP" gate="G$1" x="20.32" y="55.88" smashed="yes">
<attribute name="NAME" x="22.86" y="60.96" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="2.54" y="53.34" smashed="yes">
<attribute name="NAME" x="2.54" y="55.88" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="2.54" y="50.8" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R2" gate="G$1" x="2.54" y="63.5" smashed="yes">
<attribute name="NAME" x="2.54" y="66.04" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="2.54" y="60.96" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="2.54" y="73.66" smashed="yes">
<attribute name="NAME" x="2.54" y="76.2" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="2.54" y="71.12" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="2.54" y="83.82" smashed="yes">
<attribute name="NAME" x="2.54" y="86.36" size="1.778" layer="95" align="bottom-center"/>
<attribute name="VALUE" x="2.54" y="81.28" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="AUDIO_SW" gate="G$2" x="-58.42" y="88.9" smashed="yes"/>
<instance part="AUDIO_SW" gate="G$1" x="-55.88" y="93.98" smashed="yes">
<attribute name="NAME" x="-58.42" y="96.52" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="NANO" gate="G$1" x="-101.6" y="71.12" smashed="yes">
<attribute name="NAME" x="-88.9" y="45.72" size="1.778" layer="95" rot="R90"/>
</instance>
<instance part="PWR_SW" gate="G$2" x="-101.6" y="-10.16" smashed="yes"/>
<instance part="PWR_SW" gate="G$1" x="-99.06" y="-5.08" smashed="yes">
<attribute name="NAME" x="-101.6" y="-2.54" size="1.778" layer="95" align="bottom-right"/>
</instance>
<instance part="BUZZ" gate="G$1" x="-35.56" y="76.2" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="5V" class="0">
<segment>
<pinref part="USB" gate="G$1" pin="1_VCC"/>
<wire x1="-137.16" y1="48.26" x2="-142.24" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="48.26" x2="-142.24" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="116.84" x2="-114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="33.02" x2="-114.3" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="33.02" x2="-114.3" y2="116.84" width="0.1524" layer="91"/>
<junction x="-114.3" y="116.84"/>
<pinref part="LED_TOP" gate="G$1" pin="VCC"/>
<wire x1="55.88" y1="10.16" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<wire x1="55.88" y1="7.62" x2="45.72" y2="7.62" width="0.1524" layer="91"/>
<wire x1="45.72" y1="7.62" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<wire x1="45.72" y1="55.88" x2="45.72" y2="116.84" width="0.1524" layer="91"/>
<wire x1="45.72" y1="116.84" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<pinref part="LED_BOTTOM" gate="G$1" pin="VCC"/>
<wire x1="27.94" y1="116.84" x2="-114.3" y2="116.84" width="0.1524" layer="91"/>
<wire x1="55.88" y1="58.42" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<wire x1="55.88" y1="55.88" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<junction x="45.72" y="55.88"/>
<pinref part="UP" gate="G$1" pin="B"/>
<wire x1="27.94" y1="55.88" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
<junction x="27.94" y="116.84"/>
<pinref part="DOWN" gate="G$1" pin="B"/>
<wire x1="27.94" y1="66.04" x2="27.94" y2="76.2" width="0.1524" layer="91"/>
<wire x1="27.94" y1="76.2" x2="27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="27.94" y1="86.36" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<junction x="27.94" y="66.04"/>
<pinref part="LEFT" gate="G$1" pin="B"/>
<junction x="27.94" y="76.2"/>
<pinref part="RIGHT" gate="G$1" pin="B"/>
<junction x="27.94" y="86.36"/>
<pinref part="NANO" gate="G$1" pin="VIN"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="LED_TOP" gate="G$1" pin="DIN"/>
<wire x1="66.04" y1="10.16" x2="66.04" y2="2.54" width="0.1524" layer="91"/>
<wire x1="66.04" y1="2.54" x2="38.1" y2="2.54" width="0.1524" layer="91"/>
<wire x1="38.1" y1="2.54" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
<wire x1="38.1" y1="43.18" x2="-73.66" y2="43.18" width="0.1524" layer="91"/>
<pinref part="NANO" gate="G$1" pin="D2"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="LED_TOP" gate="G$1" pin="CS"/>
<wire x1="55.88" y1="40.64" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="45.72" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
<pinref part="LED_BOTTOM" gate="G$1" pin="CS"/>
<wire x1="40.64" y1="45.72" x2="-73.66" y2="45.72" width="0.1524" layer="91"/>
<wire x1="55.88" y1="88.9" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<wire x1="55.88" y1="91.44" x2="40.64" y2="91.44" width="0.1524" layer="91"/>
<wire x1="40.64" y1="91.44" x2="40.64" y2="45.72" width="0.1524" layer="91"/>
<junction x="40.64" y="45.72"/>
<pinref part="NANO" gate="G$1" pin="D3"/>
</segment>
</net>
<net name="CLK" class="0">
<segment>
<pinref part="LED_TOP" gate="G$1" pin="CLK"/>
<wire x1="66.04" y1="40.64" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="66.04" y1="48.26" x2="-73.66" y2="48.26" width="0.1524" layer="91"/>
<wire x1="66.04" y1="48.26" x2="78.74" y2="48.26" width="0.1524" layer="91"/>
<junction x="66.04" y="48.26"/>
<wire x1="78.74" y1="48.26" x2="78.74" y2="91.44" width="0.1524" layer="91"/>
<pinref part="LED_BOTTOM" gate="G$1" pin="CLK"/>
<wire x1="78.74" y1="91.44" x2="66.04" y2="91.44" width="0.1524" layer="91"/>
<wire x1="66.04" y1="91.44" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
<pinref part="NANO" gate="G$1" pin="D4"/>
</segment>
</net>
<net name="D2" class="0">
<segment>
<pinref part="LED_TOP" gate="G$1" pin="DOUT"/>
<wire x1="60.96" y1="40.64" x2="60.96" y2="50.8" width="0.1524" layer="91"/>
<wire x1="60.96" y1="50.8" x2="66.04" y2="50.8" width="0.1524" layer="91"/>
<pinref part="LED_BOTTOM" gate="G$1" pin="DIN"/>
<wire x1="66.04" y1="50.8" x2="66.04" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="UP" class="0">
<segment>
<pinref part="UP" gate="G$1" pin="A"/>
<wire x1="12.7" y1="55.88" x2="-5.08" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="55.88" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="55.88" x2="-7.62" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="50.8" x2="-73.66" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="53.34" x2="-5.08" y2="55.88" width="0.1524" layer="91"/>
<junction x="-5.08" y="55.88"/>
<pinref part="NANO" gate="G$1" pin="D5"/>
</segment>
</net>
<net name="DOWN" class="0">
<segment>
<pinref part="DOWN" gate="G$1" pin="A"/>
<wire x1="12.7" y1="66.04" x2="-5.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="66.04" x2="-10.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="66.04" x2="-10.16" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="53.34" x2="-73.66" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="63.5" x2="-5.08" y2="66.04" width="0.1524" layer="91"/>
<junction x="-5.08" y="66.04"/>
<pinref part="NANO" gate="G$1" pin="D6"/>
</segment>
</net>
<net name="LEFT" class="0">
<segment>
<pinref part="LEFT" gate="G$1" pin="A"/>
<wire x1="12.7" y1="76.2" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="76.2" x2="-12.7" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="76.2" x2="-12.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="55.88" x2="-73.66" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="73.66" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<junction x="-5.08" y="76.2"/>
<pinref part="NANO" gate="G$1" pin="D7"/>
</segment>
</net>
<net name="RIGHT" class="0">
<segment>
<pinref part="RIGHT" gate="G$1" pin="A"/>
<wire x1="12.7" y1="86.36" x2="-5.08" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="86.36" x2="-15.24" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="86.36" x2="-15.24" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="58.42" x2="-73.66" y2="58.42" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="83.82" x2="-5.08" y2="86.36" width="0.1524" layer="91"/>
<junction x="-5.08" y="86.36"/>
<pinref part="NANO" gate="G$1" pin="D8"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="10.16" y1="83.82" x2="10.16" y2="73.66" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="10.16" y1="73.66" x2="10.16" y2="63.5" width="0.1524" layer="91"/>
<junction x="10.16" y="73.66"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="10.16" y1="63.5" x2="10.16" y2="53.34" width="0.1524" layer="91"/>
<junction x="10.16" y="63.5"/>
<wire x1="-30.48" y1="-5.08" x2="10.16" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-5.08" x2="35.56" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<wire x1="35.56" y1="40.64" x2="35.56" y2="-5.08" width="0.1524" layer="91"/>
<junction x="35.56" y="-5.08"/>
<pinref part="LED_TOP" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="10.16" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="60.96" y1="-5.08" x2="35.56" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="LED_BOTTOM" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="58.42" x2="60.96" y2="55.88" width="0.1524" layer="91"/>
<wire x1="60.96" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<wire x1="81.28" y1="55.88" x2="81.28" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-5.08" x2="60.96" y2="-5.08" width="0.1524" layer="91"/>
<junction x="60.96" y="-5.08"/>
<wire x1="10.16" y1="53.34" x2="10.16" y2="-5.08" width="0.1524" layer="91"/>
<junction x="10.16" y="53.34"/>
<junction x="10.16" y="-5.08"/>
<wire x1="-30.48" y1="73.66" x2="-30.48" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="NANO" gate="G$1" pin="GND2"/>
<wire x1="-30.48" y1="-5.08" x2="-81.28" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-5.08" x2="-81.28" y2="0" width="0.1524" layer="91"/>
<junction x="-30.48" y="-5.08"/>
<pinref part="PWR_SW" gate="G$1" pin="A"/>
<wire x1="-81.28" y1="0" x2="-91.44" y2="0" width="0.1524" layer="91"/>
<pinref part="BUZZ" gate="G$1" pin="MINUS"/>
<wire x1="-73.66" y1="40.64" x2="-91.44" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="40.64" x2="-91.44" y2="35.56" width="0.1524" layer="91"/>
<junction x="-73.66" y="40.64"/>
<pinref part="NANO" gate="G$1" pin="GND1"/>
<wire x1="-91.44" y1="35.56" x2="-106.68" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUZZ_SW" class="0">
<segment>
<pinref part="AUDIO_SW" gate="G$1" pin="N"/>
<wire x1="-63.5" y1="93.98" x2="-68.58" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="83.82" x2="-68.58" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="68.58" x2="-73.66" y2="68.58" width="0.1524" layer="91"/>
<pinref part="NANO" gate="G$1" pin="D12"/>
</segment>
</net>
<net name="BUZZ" class="0">
<segment>
<wire x1="-30.48" y1="78.74" x2="-30.48" y2="88.9" width="0.1524" layer="91"/>
<pinref part="BUZZ" gate="G$1" pin="PLUS"/>
<pinref part="AUDIO_SW" gate="G$1" pin="B"/>
<wire x1="-30.48" y1="88.9" x2="-48.26" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND_IN" class="0">
<segment>
<pinref part="PWR_SW" gate="G$1" pin="N"/>
<wire x1="-142.24" y1="-5.08" x2="-106.68" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="38.1" x2="-142.24" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="USB" gate="G$1" pin="5_GND"/>
<wire x1="-137.16" y1="38.1" x2="-142.24" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,-58.42,88.9,AUDIO_SWG$2,GND,,,,"/>
<approved hash="101,1,-101.6,-10.16,PWR_SWG$2,GND,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
